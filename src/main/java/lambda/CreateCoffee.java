package lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import org.json.simple.JSONObject;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class CreateCoffee extends CoffeeApi implements RequestStreamHandler {

    @Override
    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context)
            throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject responseJson = new JSONObject();
        try {
            JSONObject event = (JSONObject) parser.parse(reader);

            if (event.get("body") != null) {
                Coffee coffee = CoffeeJsonUtil.fromJson((String) event.get("body"));

                String fileName = addCoffeeFileToS3(coffee);
                createResponse(responseJson, 200, "New file has been added/updated: " + fileName, coffee.toString());
            }
        } catch (Exception e) {
            createResponse(responseJson, 400, "Exception occurred.", e.getMessage());
        }

        outputStream.write(responseJson.toString().getBytes(StandardCharsets.UTF_8));
    }

    private void createResponse(JSONObject responseJson, int statusCode, String message, String details) {
        JSONObject responseBody = new JSONObject();
        responseBody.put("message", message);
        responseBody.put("details", details);

        responseJson.put("statusCode", statusCode);
        responseJson.put("headers", new JSONObject());
        responseJson.put("body", responseBody.toString());
    }

    private String addCoffeeFileToS3(Coffee coffee) {
        String blend = coffee.blend();
        String description = coffee.description();

        if (blend == null || blend.isEmpty()) {
            throw new IllegalArgumentException("The blend field cannot be empty.");
        }
        if (description == null || description.isEmpty()) {
            throw new IllegalArgumentException("The description field cannot be empty.");
        }

        String fileContent = String.format("Blend: %s%nDescription: %s", blend, description);
        String fileName = (blend.replace(" ", "_") + ".txt").toLowerCase();

        SdkBytes fileBytes = SdkBytes.fromUtf8String(fileContent);
        s3Client.putObject(PutObjectRequest.builder()
                .bucket(BUCKET_NAME)
                .key(fileName)
                .contentLength((long) fileContent.length())
                .contentType("text/plain")
                .build(), RequestBody.fromBytes(fileBytes.asByteArray()));

        return fileName;
    }
}

